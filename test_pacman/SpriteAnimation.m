//
//  ESSpriteAnimation.m
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 11.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "SpriteAnimation.h"

@implementation SpriteAnimation
@synthesize repeat;

- (id)initWithTimePerFrame:(float)time framesNamed:(NSArray *)frameNames
{
    self = [super init];
    if (self) {
        timePerFrame = time;
        frames = [NSMutableArray arrayWithCapacity:[frameNames count]];
        for (NSString *name in frameNames)
            [(NSMutableArray*)frames addObject:
             [GLKTextureLoader textureWithCGImage:[UIImage imageNamed:name].CGImage
                                          options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:GLKTextureLoaderOriginBottomLeft]
                                            error:nil]];
        elapsedTime = 0;
        repeat = YES;
        animationComplete = NO;
    }
    return self;
}

- (void)update:(NSTimeInterval)dt
{
    elapsedTime += dt;
}

- (GLKTextureInfo *)currentFrame
{
    if (!repeat && animationComplete) {
        return [frames lastObject];
    }
    
    NSInteger frameIndex = ((int)(elapsedTime/timePerFrame))%[frames count];
    if (frameIndex == frames.count - 1)
        animationComplete = YES;
    return [frames objectAtIndex:frameIndex];
}

@end
