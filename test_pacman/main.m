//
//  main.m
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 17.03.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
