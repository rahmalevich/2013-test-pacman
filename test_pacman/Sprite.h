//
//  Sprite.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 17.03.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import "Node.h"

@class SpriteAnimation;
@interface Sprite : Node {
@private
    NSString *_fileName;
    SpriteAnimation *_spriteAnimation;
}

@property (nonatomic, readonly) NSString *fileName;
@property (nonatomic, strong) SpriteAnimation *spriteAnimation;

- (id)initWithFile:(NSString *)fileName effect:(GLKBaseEffect *)effect;
- (void)setSprite:(NSString *)fileName;

@end
