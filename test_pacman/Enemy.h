//
//  SCharacter.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 07.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "Character.h"

@class EnemyBehaviour, TileMap;
@interface Enemy : Character {
@private
    EnemyBehaviour *_behaviour;
}

@property (nonatomic, assign) EnemyName name;

- (id)initWithFile:(NSString *)fileName effect:(GLKBaseEffect *)effect;
- (void)makeDecisionForPlayer:(Sprite *)player andMap:(TileMap *)map;

@end
