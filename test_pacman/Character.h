//
//  Character.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 15.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "Sprite.h"

@interface Character : Sprite 

@property (nonatomic, assign) SMoveDirection direction;

@end
