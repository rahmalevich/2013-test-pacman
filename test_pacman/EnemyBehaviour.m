//
//  EnemyBehaviour.m
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 09.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "EnemyBehaviour.h"
#import "TileMap.h"
#import "Enemy.h"

@implementation EnemyBehaviour

- (void)makeDecisionForEnemy:(Enemy *)enemy player:(Sprite *)player andMap:(TileMap *)map
{
    // 1. We define which directions we'll check
    NSInteger allDirections = sDirectionUp | sDirectionDown | sDirectionLeft | sDirectionRight;
    NSInteger directionsToCheck = allDirections;
    switch (enemy.direction) {
        case sDirectionLeft:
            directionsToCheck = directionsToCheck ^ sDirectionRight;
            break;
        case sDirectionRight:
            directionsToCheck = directionsToCheck ^ sDirectionLeft;
            break;
        case sDirectionUp:
            directionsToCheck = directionsToCheck ^ sDirectionDown;
            break;
        case sDirectionDown:
            directionsToCheck = directionsToCheck ^ sDirectionUp;
            break;
        case sDirectionNone:
            break;
    }
    
    // 2. We check availability of directions
    NSInteger directionsCounter = 0;
    SMoveDirection direction = 1;
    while (direction <= directionsToCheck) {
        if ((directionsToCheck | direction) == directionsToCheck) {
            BOOL directionAvailable = [map checkDirection:direction forSprite:enemy];
            if (!directionAvailable) {
                directionsToCheck = directionsToCheck ^ direction;
            } else {
                directionsCounter++;
            }
        }
        direction = direction << 1;
    }
    
    // 3. If available more then one direction, we choose direction with smaller line distance
    if (directionsCounter > 1) {
        SMoveDirection shortestWayDirection = sDirectionNone;
        float minDistance = 0;
        SMoveDirection direction = 1;
        while (direction <= directionsToCheck) {
            if ((directionsToCheck | direction) == directionsToCheck) {
                GLKVector2 offsetVector = GLKVector2Make(0, 0);
                switch (direction) {
                    case sDirectionUp:
                        offsetVector.y = map.tileSize.height;
                        break;
                    case sDirectionDown:
                        offsetVector.y = -map.tileSize.height;
                        break;
                    case sDirectionLeft:
                        offsetVector.x = -map.tileSize.width;
                        break;
                    case sDirectionRight:
                        offsetVector.x = map.tileSize.width;
                        break;
                    case sDirectionNone:
                        break;
                }
                GLKVector2 positionToCheck = GLKVector2Make(enemy.position.x + offsetVector.x, enemy.position.y + offsetVector.y);
                float distance = GLKVector2Length(GLKVector2Subtract(player.position, positionToCheck));
                if (shortestWayDirection == sDirectionNone || distance < minDistance) {
                    shortestWayDirection = direction;
                    minDistance = distance;
                }
            }
            direction = direction << 1;
        }
        enemy.direction = shortestWayDirection;
    } else {
        enemy.direction = directionsToCheck;
    }    
}

@end
