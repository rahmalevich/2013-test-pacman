//
//  ESSpriteAnimation.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 11.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

@interface SpriteAnimation : NSObject {
    NSArray *frames;
    float timePerFrame;
    float elapsedTime;
    BOOL repeat, animationComplete;
}

@property (nonatomic, assign) BOOL repeat;

- (id)initWithTimePerFrame:(float)timePerFrame framesNamed:(NSArray *)frameNames;
- (void)update:(NSTimeInterval)dt;
- (GLKTextureInfo *)currentFrame;

@end