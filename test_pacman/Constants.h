//
//  Constants.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 15.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#define ENEMY_SPEED     25
#define PLAYER_SPEED    50

#define CONTROL_UP      100
#define CONTROL_DOWN    101
#define CONTROL_LEFT    102
#define CONTROL_RIGHT   103

#define ENEMY_ACTIVATION_PERIOD 15
#define ENEMY_ACTIVATION_POINT  CGPointMake(9, 9)

typedef enum {
    sBlinky = 0,
    sPinky,
    sInky,
    sClyde
} EnemyName;

typedef enum {
    sTagEmpty = 0,
    sTagWall = 1,
    sTagDoor = 2,
    sTagDot = 10,
    sTagEnergizer = 11,
    sPlayerSpawnPoint = 12,
    EnemySpawnPoint = 13,
} SMapObjectTag;

typedef enum {
    sDirectionNone = 0,
    sDirectionUp = 1 << 0,
    sDirectionDown = 1 << 1,
    sDirectionLeft = 1 << 2,
    sDirectionRight = 1 << 3
} SMoveDirection;